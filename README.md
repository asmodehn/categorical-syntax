# CategoricalSyntax

Categorical Syntax tooling.

# Introduction

Although Set Theory is still used as foundational theory for mathematics, other perspectives have been brought to the table that complement it in various ways : 
- Category Theory
- Homotopy Theory
- Univalent Foundations
- etc.

The most prominent benefit being computerizing the mathematical endeavour by providing automated tools that helps us to "do math".

Some tools are still very enshrined into set theory:
- metamath
- alloy
and therefore provide a simple foundation. However their usage can be cumbersome, as their core is too simple for a human to maximise his "intuition", ie reuse what is learned in everyday life (physical intuition, language practice, etc.)

Some other tools are very complex and requires a long time to get used to them :
- TLA+
- Coq
- etc.
so much so that they are still largely unused in industry, and not taught to the general population, for the cost of learning still far outweights the potential benefits.

Here, we attempt to find a middle ground between these two extremes, and leverage human "natural intuition" (similar to Kant "implicit time and space" approach), while focusing on syntax.

That syntax should represent a category of some sort, and be fully & faithfully related to the category for semantics (which should represent some human-intuitive knowledge).

Note it is only necessary for the semantics category to be "embeddable" into the actual operational semantics (via a computer tool) for it to be useful.

# Goals

Besides the syntax to promote intuitive understanding and simplicity of reasoning, we also want here to allow someone to understand:
- specification programs (no room for uncertainty)
- distributed programs (some interpretation for space)
- self-modifying programs (some interpretation for time)
alike one understands another living entity in biology.

Probably more aspects will need to be considered later on, and they should then be added to this list.

We also strive to keep things simple, and "trained-human" understandable, and for this we want to add a few constraints, hopefully helping us to focus on simple things:
- limit aware (resources, time/space physics, measurements errors, etc.)
- natural language agnostics (not word based, attempting to not follow a specific natural language grammar, etc.). The goal would be to develop a "text interpreter" similar to a REPL for example, that would allow a human developer to interract with it via whatever text is easier, but it would be a translation from the existing binary representation on disk/memory. But debugging binary executable should be possible by specialist, without any possible ambiguity, which means formal methods are required here.


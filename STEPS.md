# FIRST STEPS

Various steps that will probably be useful in our enterprise:


## Lambda COOP

- Find a way to use https://github.com/andrejbauer/coop from Danel Ahman's paper "Runners in action" via an appropriate interface (compile binary? command flow? REPL ?)

- Determine potentially interesting concepts, and a minimal terse/binary syntax to run 'as is', interpreted from binary, so that one human looking at the binary can get an understanding of the behavior...

- Try to derive this "biological feel", making an intuitive link between the running program representation in data, and its observed realworld behavior. The interpreter has to be the "Operating System" itself in this case, just running one program on one CPU/MEMORY couple. therefore there is not "running" action, just plug-in and unplug to/from an energy source.

## Formality

- Get used to Formality language

- figure out how to make its core representation more terse / usable by lowlevel hardware instructions...

- Try to find a link between this representation of behavior, and Ahman's Runners...


## Notes:

This code can probably be adapted to various hardware devices and virtual machines via Mirage unikernel to give an even more "real world" feel...

It seems not needed in our usecase to consider "colocating" "competitive processes", which is the current mainstream OS design. We are targetting for tiny devices and cooperating processes towards one "consistent" "human-predictible" behavior. Yet these must be resistent to damage to be sustainable...
Anyway, in our current usecase, if there were competitive processes, they would use different hardware, that could therefore eventually become competitive, just like in biology.








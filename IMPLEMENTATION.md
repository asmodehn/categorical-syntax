# Implementation

On a computer, although the actual hardware implementation can vary, core mathematical and physical laws of information in our world are supposed to be valid.

Following this state of things, we can attempt to specify an implementation of such a language. So this is were we will describe its operational semantics, in abstract "computer" and "information" terms, attempting to address a somewhat "general model" of such a device.


## Existing Hardware consideration

Current language are based aroud the hardware notions of "memory" and "processor". Later on arrive the "interfaces", being with human (TTY, screen/keyboard/mouse/touch/audio) or machine (network, protocols, API, data, code), that are somwhat plugged-in using lower-level concept matching the hardware (interrupts, etc., data in memory, processor computing, etc.)

The fundamental differences between memory and processor is that one is supposed to keep information static (ie. the same for a "long" time) and the other is supposed to "change" it.

Therefore the notion of how information is represented at interfaces for conservation or change is essential. 

A good exercise is probably to specify the behavior of the interpreted language via a RISC-V CPU, leveraging its instruction set.

https://en.wikipedia.org/wiki/RISC-V#ISA_base_and_extensions

And attempting to have a simple byte <-> ascii to make debugging, at least for very important basic tools (like a utf8 editor, or a "more natural language translator"), feasible.


## TEXT - Universal Human Interface

Starting with a 8bit representation of text for simplicity, these arguments should be extended to cover utf8 for a text editor / code file viewer / REPL /  a read/write human interface.

As we have seen in FLOW, a consistent text syntax for human comprehension is essential, but we must reconcile the nature of text ( a 2d character space) with the nature of information "flow" or a "resource-aware information processing representation".

Given the unidimensional nature of a character sequence or binary encoding, we want to focus here on human interpretation, meaning that we should use the second text dimension to implicitely represent order, whereas the line should not assume any order.

As a result these would represent the same concept:


```
( 1 2 3 4 5)
```
and
```
1 2 3 4 5
```
and
```
1 2 () 3 4 5
```
and
```
() 1 2 3 4 5 ()
```

or for a list:

```
[1 2 3 4 5]
```
and
```
1
2
3
4
5
```

Notice the uncertainty of
```
1 2 3 4 5 []
```
=> Order here is not guaranteed... which implies that maybe this syntax should not be allowed, since we dont want to add implicit ordering based on the content (via order relation), but we do want to ensure uncertainty by leveraging distribution algorithms. A motivation for this could be to analyse the actions taken to mitigate spectre and other hardware dependent behaviour, which seems to indicate that utilising a uniform distribution to hide any "accidental imbalance" of the hardware used is a promising way for rigourous computation.

Another option is to have specific symbol "activate"/"deactivate" already existing and somewhat implicit ordering of text representation.


Note : To simplify our purpose and avoid falling into human bias, we should strive here to define only symbols as part of the syntax. Words should be kept for a set of "specific languages" that can be defined on top of this syntax and tooling we will define later.

Here is a proposal for multiline syntax to represent set:
```
a
b
c
```
which is just the same usual oneline semantics:
and list, which adds a newline character to indicate relative ordering
```
- a
- b
- c
```
note that this is indentation sensitive, as it may refers to the line above to match with a reference
```
a
b:
 - ba
 - bb
```
means
```
( a b:[ ba bb ] ) 
```

An interesting data description language to follow seems to be OGDL here.

Note : the text representation of code is considered **immutable** here, we want to be able to reason about it on "human timescale". But it can be modified (think version control) by itself, by registering a new version of that file. We then have a somewhat functional representation of self modifying code...



## BINARY ENCODING / CHARACTER SEQUENCE - Universal Machine Interface

An interesting aspect is also that our data/code should be unambiguously represented as an (ASCII) character sequence, or a stream of bytes. Although we have used spaces in the previous examples, it is important to note that:
- space implies that one can use word (which we want to keep for developers only, not core language)
- spaces should not be relevant if all instructions can be encoded in 1 byte. Typically one could use de bruijn indices to get rid of closed terms in a lambda, and not have to "store developer's words" internally in the representation, but might keep an external mapping (a-la unison-lang).

The goal here is to have unambiguous data transfer capabilities with very strict semantics.

Because of the nature of data transfer, we guess that most of "frozen markers", ie '<>' will not need to be transferred, data is only transferred once, or maybe it will be linked with some CRC/error checking code to signal another transfer of the same data is necessary.

Also most of '[]' might disappear, as the ordering can be considered implicit in a byte sequence. However '()' will remain to specify the "simultaneity of these".


We guess the proper transfer network protocol in our usecase here is somwehre between UDP (because we need unique identifier, likely IPv6), and TCP (because we might not need, or we might reimplement some algorithms a bit differently, taking advantage of our language features)

UDP might be good for transferring short sequences to a large number of compute devices, but order will vary. We probably would need it to be reliable though.
TCP might be good for transferring long ordered sequences to a small number of devices, with order guarantees, but time may not be as small as needed for some applications...


## INSPIRATION

Some languages already exist that may offer an interesting perspective:

- https://en.wikipedia.org/wiki/J_%28programming_language%29
- https://en.wikipedia.org/wiki/K_(programming_language)








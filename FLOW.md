# Flow syntax

Here we examine a "flow"-like syntax, based on the consideration that:
- developers have long thought in somewhat dual terms of "code" and "data"
- a physical flow can represent some operational "resource management" aspect, just like linear type would, and therefore be faithful to the intuitive energy conservation concept in physics.
- language syntax is graph-like (maybe with some restrictions depending on spoken languages ?) and humans are very well used to this structure
- there are various visual representation of such physical concept (see https://en.wikipedia.org/wiki/Bond_graph ) that one could leverage in visual programming
- Queueing Theory, which is the naively matching "time" representation in this "code-space" where data flows, seems well established.

Following this state of things, we can attempt to specify such a language. So this is were we will describe its denotational semantics, in abstract "data" and "code" terms, attempting to address a somewhat "general model" of computation.

## Existing languages

- Forth is based on stack (concatenative) syntax, which forces us to include the parsing process in the interpretation of "time", for better or worse...

- Lisp is based on trees (functional) syntax. Here parsing doesn't have to be included in our operational "time" interpretation. As a result, self-modification is usually done in "stages" (cf macros, etc.)

- TCL-like, ie imperative languages, are based on complex syntax, since the "imperative" paradigm is so intuitive, a developer still has brain power available to understand the syntax.

It seems the imperative paradigm matches well the intuitive human interpretation of the language (step by step approeach to a process inducing a change in the world). However this doesn't hold when various processes are distributed in different locations. Also self modification of languages written in this paradigm are very error-prone.

It also seems the concatenative approach might be too "fine-grained" where one has to think about parsing details, that should be "intuited" by hte developer rather than focused on.

The functional approach is left, and indeed usually brings good benefits for our specification and distribution goals, but the time interpretation is still largely undecided.


## A tentative functional approach


We wont attempt to write a grammar just now, just examine some examples to motivate the discourse.

### Code

Roughly, a usual function call is largely assimilated to a subroutine, or a command call, and is written in the following way (using ';' to separate statements/expressions for now):

- concatenative: 1 a set; 3 b set; a b add
- imperative: a:=1; b:=3; add a b;
- functional: let a 1; let b 3; add a b;

One can notice immediately the "command imperative-style" creeping in, even in functional-like syntax. This is probably because of historic precedents. However if we are interested in specification, this function can have more meanings. Like with prolog or alloy it can specify some "known" fact that we want the current program to verify for example, allowing us to build something or either to invalidate something...

This is the core of the "code" aspect, where the symbols written must specify some operational behavior in time...


## Data

Representing data in various minimalist language, is tricky, because one usually builds data using code...
But there are more "core" concept that can emerge, if we start from the "intuitive human understanding", in this case flow-based physics.
Data is then the thing that flows.

We will consider here just the very minimal "sort" of data for a small self modifying language: bytes. These can be interpreted in the language as:
- 8-list of booleans (to be extended depending on machine)
- integers in [-127..127] (to be extended depending on machine)
- characters (ASCII to start simple - but UTF8 should be the aim)

To be constructive, one must be able to structure or de-structure data. A good category theoretic concept for that is containers from Danel Ahman. It allows to embed or represent other categories inside them, and would allow us to manipulate data without having to consider the "nature" of that data. Since data is the "lowest-end" of what the human pay attention to when he is developing.


### NEList

A first simple concept to use here is therefore the NEList (homogeneous - every element is of the same "type"). A NEList of integers could then be written as : 

- Literally: [ 1 2 3 4 5 ]
- Constructed via function prefix: [] 1 2 3 4 5
- Constructed via function postfix: 1 2 3 4 5 []

Note : prefix or postfix is somethig that is still to be decided, but it is likely only one would be beneficial in reaching our goals.

Interestingly the constructed syntax is also the set of elements in that list, in the sense that all these are exactly **the same** as per containers semantics. That is, they represent the same data :
- [ [ 1 2 3 4 5 ] ]
- [ [] 1 2 3 4 5] or [ 1 2 3 4 5 [] ]
- [ 1 2 3 4 5 ]
- 1 2 3 4 5

It seems quite important at this stage to remind that these are not just "reducible to the same data", but that they are various syntax for the actual same data underneath.

IF we stick to our containers semantics it is worth noting that:
- [ 1 ] is just 1 and therefore an integer is also a list with one integer.


### NESet

Lists are implicitely ordered, so we also need for distribution purposes an unordered homogeneous container.

- ( 1 2 3 4 5 )
- ( ( 1 2 3 4 5 ) )
- ( () 1 2 3 4 5 ) or ( 1 2 3 4 5 () )
- Note : 1 2 3 4 5 is NOT a NESet as order is implicit in text, but could be converted to one from the NEList interpretation when parsing.


One can note that ordered requires possibility of duplication, whereas unordered duplication seems not so useful (one can represent it with a NESet of heterogeneous container)

It seems important here that this semantics for set be compatible with types (Martin-Loef comes to mind) for various theoretical / strict applications, in the mathematical sense.


### Record: The heterogeneous container

After detailing list (the intuitive semantics of the text syntax as data) and set (the core concept when manipulating code as data - cf set-theory and types), some difficulties arise, namely "how to refer to an element of a container". NEList could refer to its element by a position using natural numbers, and NESet could refer to its elements by a string (to emphasize the non-ordered nature).

In a somewhat related way, it appears that there are different ways to interract with this data, in a dual "game-semantics-like" way, with a functional/theoretical twist:

- accessing an element (taking, without deleting it)
- verifying an element (giving, without adding)

and the usual "imperative" interpretation, that can transform the data:

- "giving" an element (check & maybe add)
- "taking" an element (access & maybe delete)

Although these require deeper analysis (copy of information ?), we will do that later, and instead focus here on the possible syntax for an heterogeneous container

Following the functional tradition, we will consider records (as well as lists and sets) immutable, to have a solid core, and leave mutability concerns to other, more advanced constructions. 

One could then define a record like this:

- { a: 1 b: question answer: 42 }
- {} a: 1 b: question answer: 42
- a: 1 b: question answer: 42 {}
- a: 1 b: question answer: 42  ??? but it is a set without order ?

ALTERNATIVES: (significance of whitespace /vs/ index access syntax ?)
- a.1 b.question answer.42
- a:1 b:question answer:42
- ( a:1 b:question answer:42 )

Records have the same unicity and unordered semantics as a NESet.

It is important to note this is also a container (akin to Huet's zipper ?) 

One can note that we had to introduce here some additional token ":" to separate the reference with the referee. This will probably be central to the language, and therefore should remain simple and ubiquitous, for which ':' seems to be a good choice, representing some sort of equivalence or isomorphism.

Although the syntax above is unambiguous, it is not very readable. Luckily, being close to the set semantics, it can be rewritten on multiple lines for better readability (cf ONELINE further down)


### ONELINE

So far we have proposed various syntax in oneline, to provide some consistency to core data concepts. However to be able to deal with human perception of "written" language it seems useful to rely on two dimensional text (especially given the COPY aspect coming up...).

But it still need to be "simply representable" in one dimension (think serialization, network, etc.), even if this representation might be a bit heavier on the human comprehension side...

A multiline is iplicitely ordered top-down, just like a oneline is right-left, in the Western Culture. this should be "tunable", albeit just in the "text editor" or REPL used by the programer, not as part of the language itself.

But that means there is an order, and therefore:

```
1 2 3
4 5 6
```

is the same as

```
[ [ 1 2 3 ] [ 4 5 6 ] ]
```


### COPY

It is important to notice how "copy of information" which is another implicit for humans, including in mathematic practice, is completely not true in general in our world. information needs energy, and energy will not appear spontaneously where we need it, when we need it.

Formality is an example of a functional language where this observation is central to its features.

For a strict functional / mathematical approach, or even a "resources-aware" approach, this is central, but it must be translated somehow into an human-intuitive concept (hence the "flow" paradigm detailed here).

One important remark is that if we are resource aware, a "copy" implies one current location of information, that instantenously becomes two.

It also follows from this that copy and unicity cannot go together for obvious reason, since the copy must be "identical". And therefore a duplication (cardinality) "precedes" ordering, conceptualy.

That's why we propose such a syntax for duplication "as needed":
```
<a> 
```
represents a that can be duplicated as needed.

This then is tied to mutation, specifically : 
```
[ 1 2 3 ] 
```
now means it is mutable, because
```
<[ 1 2 3 ]>
```
is immutable, as it provides duplication as needed.

similarly for sets:
```
(1 2 3)
```
is mutable (but provide unicity of use, ie linearity), and
```
<(1 2 3)>
```
is immutable and provide duplication as needed.

<> then becomes the "freeze" operator, capturing the existing data, and providing potential duplication of information, at the cost of mutability.




### CODE

Although we talked about data only so far, it is important to scrutinize the usual way to transform data via code, and see how this can represent what we need, and follow an interesting syntax...

A usual function could be defined as (supposing the add function exists, we can "call" it here):
```
add42:
  <( a:a )>    # this is an immutable set -> a is not "captured" on call
             # but a cannot be mutated in here.
  a add 42   # this is a set -> no ordering 
             # -> ok for commutative& associative operations.
```
Notice the indentation that defines the scope where parameters are visible.
Also notice that:
```
a:a
```
means exactly the same as
```
a
```
that is that the 'a' variable is referenceable internally via 'a'.

Also in this example, add could be defined as:
```
add:
  ( a b )    # order is not important !
             # but generally, a list should probably be preferred... 
  a + b      # just representing a result here, nothing more.
```

One intresting aspect is the calling is made as :
- caller "a" can match callee "a" or "b"
- caller "42" can match callee "a" or "b".
This is ambiguous in calling convention, but is not a problem in this specific function definition, which the () set representation is meant to indicate.

If the order of call matters, one can : 
```
add:
  <[ a b ]>
  a + b
```
or (frozen text implying "a then b" order frozen)
```
add:
  - <a>
  - <b>
  a + b
```

Calling it then works with:
```
add 53 42
```
or
```
53 42 add
```

but not:
```
42 add 53
```
as order cannot be determined...

Note, if one specify add as:

```
add:
  [a b]
  a = a + b
```
it means that a and b ( and the list itself ) is mutable. Also implies that calling it means absorbing data from caller environment.

```
a:53

add a 42
```
=> not working as prefix syntax cannot absorb data
```
a:53

add <a> 42
```
=> works
```
a:53

a 42 add
```
=> works, and a is absorbed.

Note : this should follow similar rules as rust borrowchecker, with various kinds of variables.

Note 2: maybe the absorption is not obvious enough in text without extra representation/symbol...

On another aspect, we saw how to reference a value:
```
a:b:42
```
or, to prevent consumption by caller
```
<a:b:42>
```
which is the same as
```
<a:<b:<42>>>
```

and we can dereference like so:

```
a.b
```
value maybe being consumed (depending on how it was defined):
```
a.b
```
value not consumed (but copy has a cost ! -> extra symbol)
```
<a.b>
```

## REFERENCES

This "flow" paradigm brings to mind similar or related concepts:

- proof nets
- interaction combinators (Lafont)
- Formality Language


Note that while the flow concept is the definitional aspect of the toy level, other human intuitive concepts could be present at a "higher scale", hence we should also get inspiration from :
- unison-lang (consistent hashes for seamless distributed merges)
- pijul (patch theory)
- CRDTs
- erlang
- 







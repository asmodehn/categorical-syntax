# SCALES

We have seen before that we want a "flow" paradigm, to have some "resources awareness", and the like of linear subtyping, while leveraging some "human intuitive physics understanding".

It seems important to mention that, like explained in Danel Ahman's papers about "Runners", these can be "stacked up" like matryoshka dolls, to better deal with various real world concerns, depending on the scale we operate on.

Low : computer scale : fast time and small size
Medium : human scale : human-experienced time and human-like size
Large : population scale : population experienced time (multiple generations), and population size (multiple locations, yet where human communication is already somehow enabled)

In the future, probably more levels will be needed, but these will probably suffice for a first generation of software tools based on these concepts.

A part of our goal here seems then to be the following.
Define how we can stack up three levels of runners:
- Toy level: a very fast, very compact language that can run on a computer. (Yet its properties must be understandable by a human for predictability, debuggability, etc.)
- Human level: a machine that we can interract with as we would with another animal or human being (maybe starting with a rat, and growing to a human, as mammals seems more intuitively related to us, I guess...)
- Population level: a distributed machine that we can interract with as a population, maybe requiring multiple experts in various fields...

Interrestingly, one can realize that nowadays computers are very fuzzily spread out on this scale. One machine may require multiple experts to make it work, yet our distributed tools seems so simple to use for the average person.

A part of our argument in following this structure is that it makes the whole IT system more resilient and evolvable by being aligned to human scale, as we will arguable always need (and want) to have computer interract with us, developing it with the aim to become a symbiot at the various scales.



## Human level

Since this seems the most intuitive, it might be simpler to start with it, for hte specification part.

Possible interractions:
- Audio, Spoken language : voice recognition being worked on already.
- Pencil as a human centric tool, Written language : writing recognition being worked on already. even hardware device like tablets and phones.
- Video : camera/screen being worked on already.
- keyboards: maybe some interractive keyboard where interractions are suggested/limited by the software...

The real problem in these possible interractions is the interpreted semantics from the signals we percieve.
These should probably be our topic of interest in this scale : which tools can help us interpret human semantics here... some possible candidates:
- a (local) model of people interraction
- a model of physical interraction
so that the computer builds some "understanding" of the human experience, that it can use to better understand us and what we talk about...

At this scale a first prototype could be a "read/write tablet", behaving like an advanced interactive terminal for example, understanding human writing, and responding with writing. On the long time scale, it is a book, that became interractive, and enables seamless communication with others books.

The plan at this level is to start simple, with whatever human interface are already available (likely a simple terminal, or some simple formal graphical UI), and start developing or repurposing tools for humans to interract with minimalistic computers. The first priority has to be "understanding" here, in a way that one human can teach the complete computer behavior to another one human, while leveraging that computer for teaching in some (possibly evolving) way...

The difficulty being that the computer must behave predictibly, yet can evolve based on human usage, all this in a way that is not "unsettling" for a human. Probalb something that is biology like, evolving along the human understanding progresses while it uses the computer...

Other aspects (interfaces, technology, theories, etc.) seem to already be worked on, so we dont want to burden ourselves with these at this early stage.
This topic seems to be half human psychology, and half theoretical biology. But we do need some theoretical computer paradigm underneath to enable this level of computing...


## Toy level

This is probably the first scale we can start working on, as we could feasibly have multiple implementation of this.

At this level we are talking about ONE CPU/MEMORY couple, maybe like a RISCV device. But it could also be a sensor or an actuator, even if at the first stage it would probably be just some simple input/output device.

We want here some very strong guarantees in how this system, once scaled up by connecting many similar other systems, could behave. Maybe not in a way human can predict, but in a way another machine could predict... Homotopy Theory and Category theory seems to be the appropriate formal tools for this endaevour.

Because this is not the place where human should usually interract, yet it is a scale where some specialist could sporadically intervene to fix issues, it seems important that software at this scale behave as "biology-like" as possible, in the sense of a sustainable, self-repairing, self-adapting system.

If we take the "Runner" analogy from before, this is the lowest Runner level, and we must find way to keep it as simple, and as self-sustainable as possible. In the same way that an organic cell will self-repair when possible, but will know to self-destruct when suitable.

It is important to note that this level can be quite complex, as human intervening here are supposed to have tools to help in the diagnostics and repairs. But the core must be extremely simple so that a human can effectively predict teh suitability of hte software, and detect potential undesired side effects in its tools as well... This balance here seems to be hard to decide upon, and is likely to evolve as human knowledge advances.

It seems also important to note, that this level is the only one that is **not distributed**, but it must be distribution friendly to enable other scales, a bit like erlang is. For example, one node has the capability by default to suicide itself in order to save the whole from further damage.



## Population level

It seems important to realize that computers are already a "population sclae endaevor" as one person could never mine and assemble all physical elements needed for a computer. It seems possible in the future that technology improves, but it might also degrades, and simplification could be a nice benefit from the point of human understanding. 

At population scale, distributed computing and standards like the web come to mind, and are indeed important for inter-human cooperation, but one must also be wary of time scale of population, that is multiple generations. All specialities involved in maintaining such a distributed system must be transferable between generations, and likely the computer itself plays a large part in this knowledge transfer.

Having a common framework to build upon and talk about, in any scale, is attractive, looking at this problem through the "simplification is sustainability" perspective.

At the population level, it seems important to have a common "shared computer language" that everyone agrees upon to "try and understand things" with a set of computers, maybe akin the currently wide spread python language and the web standards.
Simplifications to provide human intuitive machine behavior, like CRDTs for example are also a nice addition to the simplification agenda.

The plan here is to stick with whatever is readily available, until the lower level, and the underlying theory is mature and well developed enough to undertake the endaevour to redefine this layer. Also it might not be an individual decision to do so, but a natural evolutive process, and some system should be thought out to enable this (like RFCs, PEP, but on the more formal and automated side...)





















 












